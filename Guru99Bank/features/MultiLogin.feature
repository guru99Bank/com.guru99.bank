Feature: This test is to determine to test the guru bank login functionality with multiple set of data.
  I want to verify the every login functinality getting successfull or not

  Background: Navigate to guru bank site
    Given I have navigated to guru Website
    Then Verify the title of the Guru Home Page

  #Scenario Outline: Verify the login functionality with mutiple data
  #When user enter the invalid username in username text "<userid>"
  #And user enter the invalid password in password text "<password>"
  #When I Click on Login button Successfull
  #Then I verify the Login should  successfull done and verify the Manager HomePage Title
  #When I Click on Logout button
  #
  #Examples:
  #| userid     | password |
  #| mngr128408 | qabatYg  |
  #| mngr129661 | ajArUzU  |
  #| mngr129662 | vEdEtUq  |
  #Scenario: Verify the login functionality with mutiple data
  #When user enter the invalid username in username text
  #| Username   |
  #| mngr128408 |
  #And user enter the invalid password in password text
  #| password |
  #| qabatYg  |
  #When I Click on Login button Successfull
  #Then I verify the Login should  successfull done and verify the Manager HomePage Title
  #When I Click on Logout button
  @SystemTest
  Scenario: Verify the login functionality with mutiple data
    When user enter the Valid username and password
      | Username   | password |
      | mngr129661 | ajArUzU  |
      | mngr129662 | vEdEtUq  |
    #When I Click on Login button Successfull
    #Then I verify the Login shoul	d  successfull done and verify the Manager HomePage Title
    #When I Click on Logout button
