package com.guru99.testRunner;
import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
@RunWith(Cucumber.class)
@CucumberOptions(
features = "features", 
glue = { "com.guru99.stepDefinitions" },
//plugin = { "pretty","html:target/cucumber"}, // report genration
 monochrome = true, 
 tags = {"@tag1"},
			
 plugin = {"pretty", "html:target/cucumber-reports", 	"json:target/cucumber.json", 
					"com.cucumber.listener.ExtentCucumberFormatter:output/report.html"}

)
public class TestRunner {
	
}
