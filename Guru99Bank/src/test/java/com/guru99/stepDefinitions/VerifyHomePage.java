package com.guru99.stepDefinitions;
import static org.testng.Assert.assertTrue;

import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.guru99.PageLibrary.LoginPage;
import com.guru99.PageLibrary.ManagerHomePage;
import com.guru99.testBase.TestBase;
/**
 * Verify all the test Steps from Home Page
 * @author rameshk
 *
 */
public class VerifyHomePage extends TestBase {
	LoginPage loginPage;
	ManagerHomePage managerHomePage;
	@BeforeClass
	public void setup() throws IOException{
		initialize();
	
		
	}
	@Test
	public void verifyLoginPageWithValidCred(){
		try {
			loginPage = new LoginPage(driver);
			Assert.assertEquals(loginPage.getTitle(), "Guru99 Bank Home Page");
			assertTrue(loginPage.isGuru99BankHeaderDisplayed());
			if (loginPage.isUsernameLableDisplayed() && loginPage.isPasswordlableDisplayed()) {
				loginPage.setUsername(prop.getProperty("username"));
				loginPage.setPassword(prop.getProperty("password"));
			}
			if (loginPage.isLoginButtonDisplayed()) {
				managerHomePage = loginPage.clickloginBtn();
				Assert.assertEquals(managerHomePage.getTitle(), "Guru99 Bank Manager HomePage");
			}
		
		} catch (Exception e) {
			// TODO: handle exception
			//screenShot("verifyLoginPageWithValidCred");
		}
		
		
		
	}
}
