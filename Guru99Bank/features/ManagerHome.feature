Feature: Verify the End to end functionality in Manager homepage

  Background: Login to guru99
    Given I have navigated to guru Website
    Then Verify the title of the Guru Home Page
    When user enter the invalid username in username text field "mngr129661"
    And user enter the invalid password in password text field "ajArUzU"
    When I Click on Login button Successfull
    Then I verify the Login should  successfull done and verify the Manager HomePage Title


  Scenario: Verify the Manager ID from Manger Home Page
    Then Verify the Title of the Manager HomePage
    Then Verify the ManagerId
    
    
