package com.guru99.testBase;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.Alert;
import org.openqa.selenium.ElementNotVisibleException;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.NoSuchFrameException;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class TestBase {
	public static Properties prop;
	public static WebDriver driver;
	public static FileInputStream fis;

	/**
	 * This method is to determine the reading the property file.
	 * 
	 * @throws IOException
	 */
	public void properties() throws IOException {
		prop = new Properties();
		try {
			fis = new FileInputStream(
					System.getProperty("user.dir") + "/src/test/java/com/guru99/config/config.properties");
			prop.load(fis);
			System.out.println("Properties from config file " + prop);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public void initialize() throws IOException {
		properties();
		invokeBrowser(prop.getProperty("browerName"));
		driver.get(prop.getProperty("siteurl"));
		driver.manage().window().maximize();
	}

	/**
	 * Invoke the browser for all levels.
	 * 
	 * @param browser
	 */
	public void invokeBrowser(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.firefox.marionette", "C:\\Users\\BigData\\git\\guru99BankTest\\Guru99Bank\\drivers\\geckodriver.exe");
			FirefoxOptions options = new FirefoxOptions();
			options.setBinary("C:\\Program Files (x86)\\Mozilla Firefox\\firefox.exe"); // This

			driver = new FirefoxDriver(options);
		} else if (browser.equalsIgnoreCase("chrome")) {

			System.setProperty("webdriver.chrome.driver", "C:\\Users\\BigData\\git\\guru99BankTest\\Guru99Bank\\drivers\\chromedriver.exe");
			driver = new ChromeDriver();
		} else if (browser.equalsIgnoreCase("Iexplore")) {
			// System.setProperty("webdriver.chrome.driver",
			// "C:\\Users\\rameshk\\Downloads\\Guru99Bank\\drivers\\chromedriver.exe");
			System.setProperty("webdriver.ie.driver", "");
			driver = new InternetExplorerDriver();
		} else {
			// driver = new HtmlUnitDriver();

		}
	}

	public void acceptAlert() {
		Alert al = driver.switchTo().alert();
		al.accept();
	}

	public void dismissAlert() {
		Alert al = driver.switchTo().alert();
		al.dismiss();
	}

	public Object executeScript(String script) {
		JavascriptExecutor exe = (JavascriptExecutor) driver;

		return exe.executeScript(script);
	}

	public Object executeScript(String script, Object... args) {
		JavascriptExecutor exe = (JavascriptExecutor) driver;

		return exe.executeScript(script, args);
	}

	public void scrollToElemet(WebElement element) {
		executeScript("window.scrollTo(arguments[0],arguments[1])", element.getLocation().x, element.getLocation().y);

	}

	public void scrollToElemetAndClick(WebElement element) {
		scrollToElemet(element);
		element.click();

	}

	public void scrollIntoView(WebElement element) {
		executeScript("arguments[0].scrollIntoView()", element);

	}

	public void scrollIntoViewAndClick(WebElement element) {
		scrollIntoView(element);
		element.click();

	}

	public void scrollDownVertically() {
		executeScript("window.scrollTo(0, document.body.scrollHeight)");
	}

	public void scrollUpVertically() {
		executeScript("window.scrollTo(0, -document.body.scrollHeight)");
	}

	public void scrollDownByPixel() {
		executeScript("window.scrollBy(0,1500)");
	}

	public void scrollUpByPixel() {
		executeScript("window.scrollBy(0,-1500)");
	}

	public void ZoomInBypercentage() {
		executeScript("document.body.style.zoom='40%'");
	}

	public void ZoomBy100percentage() {
		executeScript("document.body.style.zoom='100%'");
	}

	public void goBack() {
		driver.navigate().back();

	}

	public void goForward() {
		driver.navigate().forward();

	}

	public void refresh() {
		driver.navigate().refresh();

	}

	public void switchToFrame(String nameOrId) {
		driver.switchTo().frame(nameOrId);

	}

	private WebDriverWait getWait(int timeOutInSeconds, int pollingEveryInMiliSec) {

		WebDriverWait wait = new WebDriverWait(driver, timeOutInSeconds);
		wait.pollingEvery(pollingEveryInMiliSec, TimeUnit.MILLISECONDS);
		wait.ignoring(NoSuchElementException.class);
		wait.ignoring(ElementNotVisibleException.class);
		wait.ignoring(StaleElementReferenceException.class);
		wait.ignoring(NoSuchFrameException.class);
		return wait;
	}

	public void waitForElementVisible(WebElement locator, int timeOutInSeconds, int pollingEveryInMiliSec) {

		WebDriverWait wait = getWait(timeOutInSeconds, pollingEveryInMiliSec);
		wait.until(ExpectedConditions.visibilityOf(locator));
	}

	public void waitForElementVisibleOf(WebDriver driver, WebElement element, long timeout) {
		WebDriverWait wait = new WebDriverWait(driver, timeout);
		wait.until(ExpectedConditions.visibilityOf(element));

	}

	public WebElement waitForElementToBeClickable(WebDriver driver, long time, WebElement element) {
		WebDriverWait wait = new WebDriverWait(driver, time);
		return wait.until(ExpectedConditions.elementToBeClickable(element));
	}
}
