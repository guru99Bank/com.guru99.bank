@functionalTest
Feature: Verify the end 2 end Login functionality from guru website
   You can write only one featur in a feature file but you can write multiple scenarios in single feature file
@SmokeTest
  Scenario: Verify the GUI of guru Login page
    Given I have navigated to guru Website
    Then Verify the title of the Guru Home Page
    Then Verify the Username and Password label
    Then Verify the Username and password text fields
    Then Verify the Login and reset buttons
@SystemTest  @RegTest
  Scenario: Verify the guru login functionality with empty details
    Given I have navigated to guru Website
    Then Verify the title of the Guru Home Page
    When User clear the existing data from username text field.
    When User clear the existing data from password text field.
    And Click on Login button
    Then Verify the User should not be able to login and throw a validation message
@RegTest
  Scenario: Verify the guru login functionality with invalid credentials
    Given I have navigated to guru Website
    Then Verify the title of the Guru Home Page
    When user enter the invalid username in username text field "jhsgfjgjf"
    And user enter the invalid password in password text field "sdsutugjs"
    When I Click on Login button
    Then I verify the Login should not successfull and throw validation message
@RegTest
  Scenario: Verify the guru login functionality with valid credentials
    Given I have navigated to guru Website
    Then Verify the title of the Guru Home Page
    When user enter the invalid username in username text field "mngr129661"
    And user enter the invalid password in password text field "ajArUzU"
    When I Click on Login button Successfull
    Then I verify the Login should  successfull done and verify the Manager HomePage Title
    
