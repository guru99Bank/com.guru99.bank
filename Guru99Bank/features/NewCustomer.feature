@functionalTest
Feature: Verify the End to end functionality in Manager homepage

  Background: Login to guru99
    Given I have navigated to guru Website
    Then Verify the title of the Guru Home Page
    When user enter the invalid username in username text field "mngr129661"
    And user enter the invalid password in password text field "ajArUzU"
    When I Click on Login button Successfull
    Then I verify the Login should  successfull done and verify the Manager HomePage Title

  @SmokeTest @RegTest
  Scenario: Verify the Add New Customer Add  customer  Page
    Given I have clicking  to New Customer
    When user enter the valid  Customer Name in Customer Name text field "BillGates"
    Then user select male from Radiobutton
    And user enter the date of birth from DOB text field "23/05/1971"
    And user enter the Address fields from Address text box "Stephen"
    And user enter the city fields from city text box "kurnool"
    Then user enter the state fields  from state text box "Andhra pradesh"
    Then user enter the Pin fields from pincode text box "518385"
    Then user enter the Mobile Number from Mobile Number text box "64654654"
    Then user enter the Email from Email text box "raghuramraja001@gmail.com"
    Then user enter the password from password text box "33331d"
    When I Click on Submit button Successfull
    Then I verify the Added New  Customer  successfully
