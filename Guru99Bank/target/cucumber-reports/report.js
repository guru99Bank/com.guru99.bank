$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("ExcelData.feature");
formatter.feature({
  "line": 1,
  "name": "Excel Data Reader",
  "description": "",
  "id": "excel-data-reader",
  "keyword": "Feature"
});
formatter.scenarioOutline({
  "line": 4,
  "name": "USer Login Scenario",
  "description": "",
  "id": "excel-data-reader;user-login-scenario",
  "type": "scenario_outline",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "User is at login page of the application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "User login with following username and password \"\u003cTextCase\u003e\"",
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Click on Login Buttons",
  "keyword": "Then "
});
formatter.examples({
  "line": 9,
  "name": "",
  "description": "",
  "id": "excel-data-reader;user-login-scenario;",
  "rows": [
    {
      "cells": [
        "TextCase"
      ],
      "line": 10,
      "id": "excel-data-reader;user-login-scenario;;1"
    },
    {
      "cells": [
        "Case1"
      ],
      "line": 11,
      "id": "excel-data-reader;user-login-scenario;;2"
    }
  ],
  "keyword": "Examples"
});
formatter.scenario({
  "line": 11,
  "name": "USer Login Scenario",
  "description": "",
  "id": "excel-data-reader;user-login-scenario;;2",
  "type": "scenario",
  "keyword": "Scenario Outline",
  "tags": [
    {
      "line": 3,
      "name": "@tag1"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "User is at login page of the application",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "User login with following username and password \"Case1\"",
  "matchedColumns": [
    0
  ],
  "keyword": "When "
});
formatter.step({
  "line": 7,
  "name": "Click on Login Buttons",
  "keyword": "Then "
});
formatter.match({
  "location": "LoginExcelReader.user_is_at_login_page_of_the_application()"
});
formatter.result({
  "duration": 23576602029,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "Case1",
      "offset": 49
    }
  ],
  "location": "LoginExcelReader.user_login_with_following_username_and_password(String)"
});
formatter.result({
  "duration": 3231588834,
  "error_message": "java.lang.NullPointerException\r\n\tat com.guru99.stepDefinitions.LoginExcelReader.user_login_with_following_username_and_password(LoginExcelReader.java:27)\r\n\tat ✽.When User login with following username and password \"Case1\"(ExcelData.feature:6)\r\n",
  "status": "failed"
});
formatter.match({
  "location": "LoginExcelReader.click_on_Login_Buttons()"
});
formatter.result({
  "status": "skipped"
});
formatter.embedding("image/png", "embedded0.png");
formatter.after({
  "duration": 1519558796,
  "status": "passed"
});
});