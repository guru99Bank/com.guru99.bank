package com.guru99.stepDefinitions;
import org.testng.Assert;
import com.guru99.PageLibrary.Addcustomerpage;
import com.guru99.PageLibrary.ManagerHomePage;
import com.guru99.testBase.TestBase;
import com.guru99.utils.WaitUtil;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class VerifyGuru99NewCustomerTest  extends TestBase{
	
    ManagerHomePage managerHomePage = new ManagerHomePage(driver);
	 Addcustomerpage customerpage;
	
	@Given("^I have clicking  to New Customer$")
	public void i_have_clicking_to_New_Customer() throws Throwable {
		
		     customerpage = new Addcustomerpage(driver);
		     WaitUtil.waitForPageToLoad(60,driver);
	         customerpage = managerHomePage.clicknewcustomer();
	}

	@When("^user enter the valid  Customer Name in Customer Name text field \"([^\"]*)\"$")
	public void user_enter_the_valid_Customer_Name_in_Customer_Name_text_field(String arg1) throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
		customerpage.Addcustomerpagetextbox(arg1);
	}

	@Then("^user select male from Radiobutton$")
	public void user_select_male_from_Radiobutton() throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
		customerpage.selectinggender();

	}

	@Then("^user enter the date of birth from DOB text field \"([^\"]*)\"$")
	public void user_enter_the_date_of_birth_from_DOB_text_field(String arg1) throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
		customerpage.enterthedataofbrith(arg1);
	    
	}

	@Then("^user enter the Address fields from Address text box \"([^\"]*)\"$")
	public void user_enter_the_Address_fields_from_Address_text_box(String arg1) throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
		customerpage.Adress(arg1);
	}

	@Then("^user enter the city fields from city text box \"([^\"]*)\"$")
	public void user_enter_the_city_fields_from_city_text_box(String arg1) throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
		customerpage.entercity(arg1);
	    
	}

	@Then("^user enter the state fields  from state text box \"([^\"]*)\"$")
	public void user_enter_the_state_fields_from_state_text_box(String arg1) throws Throwable {
		customerpage.enterstate(arg1);
	    
	}

	@Then("^user enter the Pin fields from pincode text box \"([^\"]*)\"$")
	public void user_enter_the_Pin_fields_from_pincode_text_box(String arg1) throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
		customerpage.enterpin(arg1);
	    
	}

	@Then("^user enter the Mobile Number from Mobile Number text box \"([^\"]*)\"$")
	public void user_enter_the_Mobile_Number_from_Mobile_Number_text_box(String arg1) throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
		customerpage.entermobilenumber(arg1);
	    
	}

	@Then("^user enter the Email from Email text box \"([^\"]*)\"$")
	public void user_enter_the_Email_from_Email_text_box(String arg1) throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
		customerpage.EnterEmail(arg1);
	}

	@Then("^user enter the password from password text box \"([^\"]*)\"$")
	public void user_enter_the_password_from_password_text_box(String arg1) throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
		customerpage.enterpassword(arg1);
	    
	}

	@When("^I Click on Submit button Successfull$")
	public void i_Click_on_Submit_button_Successfull() throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
		customerpage.clickingsubmitbutton();
		
	}

	@Then("^I verify the Added New  Customer  successfully$")
	public void i_verify_the_Added_New_Customer_successfully() throws Throwable {
		WaitUtil.waitForPageToLoad(60,driver);
	String Mess=customerpage.verifycustomersucess();
		System.out.println("Start"+Mess);
		try{
			WaitUtil.waitForPageToLoad(60,driver);
			Assert.assertEquals(Mess, "Customer Registered Successfully!!!");
		}catch(Exception e){
			System.out.println("Fail");
		}
		
	}
	
	
}
