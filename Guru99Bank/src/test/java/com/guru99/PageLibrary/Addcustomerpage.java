package com.guru99.PageLibrary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Addcustomerpage {

	 WebDriver driver;

	public Addcustomerpage(WebDriver driver) {
		this.driver = driver;
	}
	public By cicknewcustomer=By.xpath("html/body/div[3]/div/ul/li[2]/a");
	public By Customername = By.xpath("//input[@name='name']");
	public By Gender = By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[5]/td[2]/input[1]");
	public By DateofBirth = By.xpath(".//*[@id='dob']");
	public By Address =By.xpath("//textarea");
	public By City=By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[8]/td[2]/input");
	public By State=By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[9]/td[2]/input");
	public By PIN =By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[10]/td[2]/input");
	public By MobileNumber=By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[11]/td[2]/input");
	public By Email=By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[12]/td[2]/input");
	public By password=By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[13]/td[2]/input");
	public By Submitbutton=By.xpath("html/body/table/tbody/tr/td/table/tbody/tr[14]/td[2]/input[1]");
	public By Verifycustomerreg=By.xpath(".//*[@id='customer']/tbody/tr[1]/td/p");
	
	
	public void AddNewCustomer(){
		driver.findElement(cicknewcustomer).click();
	}
	
	public void Addcustomerpagetextbox(String cust){
		driver.findElement(Customername).sendKeys(cust);
		
		
	}
	
   public void 	selectinggender(){
	   driver.findElement(Gender).click();
   }
	
	public void enterthedataofbrith(String brith){
		driver.findElement(DateofBirth).sendKeys(brith);
	}

	public void Adress(String Add){
		driver.findElement(Address).sendKeys(Add);
		
	}
	
	public void entercity(String city){
		driver.findElement(City).sendKeys(city);
	}
	
	
	public void enterstate(String state){
		
		driver.findElement(State).sendKeys(state);
		
		
     }
	
	
	public void enterpin(String pin){
		driver.findElement(PIN).sendKeys(pin);
	}
	
	
	public void entermobilenumber(String mobileNumber){
		driver.findElement(MobileNumber).sendKeys(mobileNumber);
		
	}
	
	
	public void EnterEmail(String email){
		driver.findElement(Email).sendKeys(email);
	}
	
	
	public void enterpassword(String pass){
		driver.findElement(password).sendKeys(pass);
	}
	
	public void clickingsubmitbutton(){
		driver.findElement(Submitbutton).click();
	}
	
	public String  verifycustomersucess(){
		return driver.findElement(Verifycustomerreg).getText();
	}
	
	
}
