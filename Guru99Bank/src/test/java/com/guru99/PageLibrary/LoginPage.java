package com.guru99.PageLibrary;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;

public class LoginPage {

	WebDriver driver;
	/**
	 * Define your page WebUI elements.
	 */
	// Username
	By uname = By.name("uid");
	// Password
	By password = By.name("password");
	
	//login button
	By loginbtn = By.name("btnLogin");
    By resetbtn = By.name("btnReset");
	By guru99header = By.xpath("/html/body/div[2]/h2");
	By unamelbl = By.xpath("/html/body/form/table/tbody/tr[1]/td[1]");
	By passlbl = By.xpath("/html/body/form/table/tbody/tr[2]/td[1]");
	/**
	 * Constructor driver
	 */
	public LoginPage(WebDriver driver){
		this.driver = driver;
	}
	/**
	 * set the Username
	 */
	public void setUsername(String username){
		driver.findElement(uname).sendKeys(username);
	}
	
	public void setPassword(String pwd){
		driver.findElement(password).sendKeys(pwd);
	}
	
	public ManagerHomePage clickloginBtn(){
		driver.findElement(loginbtn).click();
		return new ManagerHomePage(driver);
	}
	
	public String getTitle(){
		return driver.getTitle();
				
	}
	public boolean isGuru99BankHeaderDisplayed() {
		// TODO Auto-generated method stub
		return driver.findElement(guru99header).isDisplayed();
	}
	public boolean isUsernameLableDisplayed() {
		// TODO Auto-generated method stub
		return driver.findElement(unamelbl).isDisplayed();
	}
	public boolean isPasswordlableDisplayed() {
		// TODO Auto-generated method stub
		return driver.findElement(passlbl).isDisplayed();
	}
	public boolean isLoginButtonDisplayed() {
		// TODO Auto-generated method stub
		return driver.findElement(loginbtn).isDisplayed();
	}
	public String getusernameLabel() {
		// TODO Auto-generated method stub
		return driver.findElement(unamelbl).getText();
	}

	public String getpasswordLabel() {
		// TODO Auto-generated method stub
		return driver.findElement(passlbl).getText();
	}
	public boolean isuserNametextfieldDisplyed() {
		// TODO Auto-generated method stub
		return driver.findElement(uname).isDisplayed();
	}
	public boolean ispasswordtextfieldDisplyed() {
		// TODO Auto-generated method stub
		return driver.findElement(password).isDisplayed();
	}
	public boolean isresetButtonDisplayed() {
		// TODO Auto-generated method stub
		return driver.findElement(resetbtn).isDisplayed();
	}
	public void enterUserId(String uid) {
		// TODO Auto-generated method stub
		driver.findElement(uname).sendKeys(uid);
	}
	public void clearUserIdtxtfield(){
		
		driver.findElement(uname).clear();
	}
	public void clearpasswordtxtfield() {
		// TODO Auto-generated method stub
		driver.findElement(password).clear();
		
	}
	public void enterPassword(String pwd) {
		// TODO Auto-generated method stub
		driver.findElement(password).sendKeys(pwd);
	}
	
}
