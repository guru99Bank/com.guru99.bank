package com.guru99.classObjects;

public class NewCustomer {
	public String customerName;
	public String dateofBirth;
	public String emailAddress;
	public Address address;
	
	public PhoneNumber phoneNumber;

	public class Address {
		public String streetAddress;
		public String city;
		public String postCode;
		public String state;
		public String country;
		public String county;
	}

	public class PhoneNumber {
		public String home;
		public String mob;
	}

}
