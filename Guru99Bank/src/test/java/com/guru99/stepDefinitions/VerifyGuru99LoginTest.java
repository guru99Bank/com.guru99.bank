package com.guru99.stepDefinitions;
import static org.testng.Assert.assertTrue;
import java.util.List;
import java.util.Map;
import org.openqa.selenium.Alert;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.testng.Assert;
import com.guru99.PageLibrary.LoginPage;
import com.guru99.PageLibrary.ManagerHomePage;
import com.guru99.classObjects.UserCredentials;
import com.guru99.testBase.TestBase;
import com.guru99.utils.WaitUtil;

import cucumber.api.DataTable;
import cucumber.api.Scenario;
import cucumber.api.java.After;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
public class VerifyGuru99LoginTest extends TestBase {

	static LoginPage loginPage;
	static ManagerHomePage managerHomePage;
    
	/**
	 * Scenario: Verify the GUI of guru Login page
	 * 
	 * @throws Throwable
	 */
	@Given("^I have navigated to guru Website$")
	public void i_have_navigated_to_guru_Website() throws Throwable {
		initialize();

	}

	@Then("^Verify the title of the Guru Home Page$")
	public void verify_the_title_of_the_Guru_Home_Page() throws Throwable {
		loginPage = new LoginPage(driver);
		try {
			WaitUtil.waitForPageToLoad(30,driver);
			Assert.assertEquals(loginPage.getTitle(), "Guru99 Bank Home Page");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Then("^Verify the Username and Password label$")
	public void verify_the_Username_and_Password_label() throws Throwable {
		try {
			WaitUtil.waitForPageToLoad(60,driver);
			
			if (loginPage.isUsernameLableDisplayed()) {
				Assert.assertEquals(loginPage.getusernameLabel(), "UserID");
				
				WaitUtil.waitForPageToLoad(60,driver);
				if (loginPage.isPasswordlableDisplayed()) {
					Assert.assertEquals(loginPage.getpasswordLabel(), "Password");
				}
			}
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Then("^Verify the Username and password text fields$")
	public void verify_the_Username_and_password_text_fields() throws Throwable {
		try {
			WaitUtil.waitForPageToLoad(60,driver);
			assertTrue(loginPage.isuserNametextfieldDisplyed(), "Username text field is not displyed");
			assertTrue(loginPage.ispasswordtextfieldDisplyed(), "password text field is not displyed");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	@Then("^Verify the Login and reset buttons$")
	public void verify_the_Login_and_reset_buttons() throws Throwable {
		try {
			WaitUtil.waitForPageToLoad(60,driver);
			assertTrue(loginPage.isLoginButtonDisplayed(), "Login button is not displayed");
			assertTrue(loginPage.isresetButtonDisplayed(), "reset button is not displayed");

		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}

	/**
	 * Scenario: Verify the guru login functionality with empty details
	 * 
	 * @throws Throwable
	 */
	@When("^User clear the existing data from username text field\\.$")
	public void user_clear_the_existing_data_from_username_text_field() throws Throwable {
		try {
			WaitUtil.waitForPageToLoad(60, driver);
			if (loginPage.isuserNametextfieldDisplyed()) {
				loginPage.clearUserIdtxtfield();
			}
		} catch (Exception e) {
			// TODO: handle exceptione.
			e.printStackTrace();
		}

	}

	@When("^User clear the existing data from password text field\\.$")
	public void user_clear_the_existing_data_from_password_text_field() throws Throwable {
		try {
			WaitUtil.waitForPageToLoad(60, driver);
			if (loginPage.ispasswordtextfieldDisplyed()) {
				loginPage.clearpasswordtxtfield();
			}
		} catch (Exception e) {
			// TODO: handle exceptione.
			e.printStackTrace();
		}

	}

	@When("^Click on Login button$")
	public void click_on_Login_button() throws Throwable {
		if (loginPage.isLoginButtonDisplayed()) {
			loginPage.clickloginBtn();
		}

	}

	@Then("^Verify the User should not be able to login and throw a validation message$")
	public void verify_the_User_should_not_be_able_to_login_and_throw_a_validation_message() throws Throwable {
		Alert al = driver.switchTo().alert();
		System.out.println("Login not success message  -" + al.getText());
		al.accept();

		try {
			Assert.assertEquals(loginPage.getTitle(), "Guru99 Bank Home Page");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	/**
	 * Scenario: Verify the guru login functionality with invalid credentials
	 * 
	 * @throws Throwable
	 */
	@When("^user enter the invalid username in username text field \"([^\"]*)\"$")
	public void user_enter_the_invalid_username_in_username_text_field(String uid) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		if (loginPage.isuserNametextfieldDisplyed()) {
			loginPage.enterUserId(uid);
		}
	}

	@When("^user enter the invalid password in password text field \"([^\"]*)\"$")
	public void user_enter_the_invalid_password_in_password_text_field(String pwd) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		if (loginPage.ispasswordtextfieldDisplyed()) {
			loginPage.enterPassword(pwd);
		}
	}

	@When("^I Click on Login button$")
	public void i_Click_on_Login_button() throws Throwable {
		if (loginPage.isLoginButtonDisplayed()) {
			loginPage.clickloginBtn();
		}

	}

	@Then("^I verify the Login should not successfull and throw validation message$")
	public void i_verify_the_Login_should_not_successfull_and_throw_validation_message() throws Throwable {

		Alert al = driver.switchTo().alert();
		System.out.println("Login not success message  -" + al.getText());
		al.accept();

		try {
			Assert.assertEquals(loginPage.getTitle(), "Guru99 Bank Home Page");
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
	}

	@When("^I Click on Login button Successfull$")
	public void i_Click_on_Login_button_Successfull() throws Throwable {
		if (loginPage.isLoginButtonDisplayed()) {
			managerHomePage = loginPage.clickloginBtn();
			Thread.sleep(2000);

		}
	}

	@Then("^I verify the Login should  successfull done and verify the Manager HomePage Title$")
	public void i_verify_the_Login_should_successfull_done_and_verify_the_Manager_HomePage_Title() throws Throwable {
		Assert.assertEquals(managerHomePage.getTitle(), "Guru99 Bank Manager HomePage");
	}

	@Then("^Verify the Title of the Manager HomePage$")
	public void verify_the_Title_of_the_Manager_HomePage() throws Throwable {

		Thread.sleep(2000);
		Assert.assertEquals(managerHomePage.getTitle(), "Guru99 Bank Manger HomePage");

	}

	@Then("^Verify the ManagerId$")
	public void verify_the_ManagerId() throws Throwable {

		String managerId = managerHomePage.getManagerId();
		System.out.println("ManagerID - " + managerId);

	}

	@When("^user enter the invalid username in username text \"([^\"]*)\"$")
	public void user_enter_the_invalid_username_in_username_text(String uid) throws Throwable {

		if (loginPage.isuserNametextfieldDisplyed()) {
			loginPage.enterUserId(uid);
		}
	}

	@When("^user enter the invalid password in password text \"([^\"]*)\"$")
	public void user_enter_the_invalid_password_in_password_text(String pwd) throws Throwable {

		if (loginPage.ispasswordtextfieldDisplyed()) {
			loginPage.enterPassword(pwd);
		}
	}

	@When("^I Click on Logout button$")
	public void i_Click_on_Logout_button() throws Throwable {

		loginPage = managerHomePage.logout();
		driver.switchTo().alert().accept();
		Thread.sleep(3000);
	}

	/**
	 * parameterization - Using data table -
	 * 
	 * @param scenario
	 */

	@When("^user enter the invalid username in username text$")
	public void user_enter_the_invalid_username_in_username_text(DataTable uname) throws Throwable {
		// Write code here that turns the phrase above into concrete actions
		// For automatic transformation, change DataTable to one of
		// List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
		// E,K,V must be a scalar (String, Integer, Date, enum etc)
		List<Map<String, String>> data = uname.asMaps(String.class, String.class);
		loginPage.enterUserId(data.get(0).get("Username"));

	}

	@When("^user enter the invalid password in password text$")
	public void user_enter_the_invalid_password_in_password_text(DataTable pwd) throws Throwable {

		Thread.sleep(2000);
		List<Map<String, String>> data = pwd.asMaps(String.class, String.class);
		loginPage.enterPassword(data.get(0).get("password"));
	}

	@When("^user enter the Valid username and password$")
	public void user_enter_the_Valid_username_and_password(List<UserCredentials> loginCredentials) throws Throwable {

		for (UserCredentials data : loginCredentials) {
			loginPage.enterUserId(data.getUsername());
			loginPage.enterPassword(data.getPassword());
			managerHomePage = loginPage.clickloginBtn();
			Thread.sleep(2000);
			Assert.assertEquals(managerHomePage.getTitle(), "Guru99 Bank Manager HomePage");
			loginPage = managerHomePage.logout();
			driver.switchTo().alert().accept();
			Thread.sleep(2000);
		}

	}

	@After
	public void tearDown(Scenario scenario) {
		
		if (scenario.isFailed()) {
			// Take a screenshot...
			final byte[] screenshot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
			scenario.embed(screenshot, "image/png"); // ... and embed it in the
														// report.
		}
		// driver.close();
		// driver.quit();
	}
}
