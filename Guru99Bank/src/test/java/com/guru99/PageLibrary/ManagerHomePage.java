package com.guru99.PageLibrary;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class ManagerHomePage {
	WebDriver driver;
	By managerID = By.xpath("/html/body/table/tbody/tr/td/table/tbody/tr[3]/td");
	public By cicknewcustomer=By.xpath("html/body/div[3]/div/ul/li[2]/a");
	
	By logoutlnk = By.linkText("Log out");
	public ManagerHomePage(WebDriver driver) {
	 this.driver= driver;
	}
	public String getTitle() {
		// TODO Auto-generated method stub
		return driver.getTitle();
	}
	public String getManagerId() {
		// TODO Auto-generated method stub
		return driver.findElement(managerID).getText();
	}
	
	public LoginPage logout(){
		
		driver.findElement(logoutlnk).click();
		return new LoginPage(driver);
	}
	public Addcustomerpage  clicknewcustomer(){
		driver.findElement(cicknewcustomer).click();
		return new Addcustomerpage(driver);
	}
	
}
